# API

The API will be here.

Refer to the [Getting Started Guide](https://api-platform.com/docs/distribution) for more information.

```
    composer install
```

### Create Database
```
    php bin/console doctrine:database:create
```

### Create Schema
```
    php bin/console doctrine:schema:create
```

### Run Server
```
   php bin/console server:run
```

open localhost:8000

## Behat
Cek status behat
```
   ..\vendor\bin\behat -V
```
run behat, pastikan APP_ENV=test sebelum menjalankan behat
```
   ..\vendor\bin\behat
```
