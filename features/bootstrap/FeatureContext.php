<?php

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    // public function __construct()
    // {
    // }

    /**
     * @Given Melani memiliki pekerjaan yang harus dimasukan ke sistem dengan judul :arg1
     */
    public function melaniMemilikiPekerjaanYangHarusDimasukanKeSistemDenganJudul($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When Melani memasukan pekerjaan:
     */
    public function melaniMemasukanPekerjaan(PyStringNode $string)
    {
        throw new PendingException();
    }

    /**
     * @Then Sistem menyimpan pekerjaan tersebut dan menampilkan pesan :arg1
     */
    public function sistemMenyimpanPekerjaanTersebutDanMenampilkanPesan($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Given Melani ingin melihat daftar pekerjaannya yang ada di sistem
     */
    public function melaniInginMelihatDaftarPekerjaannyaYangAdaDiSistem()
    {
        throw new PendingException();
    }

    /**
     * @When Melani melakukan GET request ke sistem :arg1
     */
    public function melaniMelakukanGetRequestKeSistem($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then Sistem menampilkan daftar pekerjaan tersebut dan menampilkan pesan :arg1
     */
    public function sistemMenampilkanDaftarPekerjaanTersebutDanMenampilkanPesan($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Given Melani menginginkan merubah sebuah pekerjaan yang telah dimasukan ke sistem dengan judul :arg1
     */
    public function melaniMenginginkanMerubahSebuahPekerjaanYangTelahDimasukanKeSistemDenganJudul($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When Melani memasukan pekerjaan yang akan di rubah:
     */
    public function melaniMemasukanPekerjaanYangAkanDiRubah(PyStringNode $string)
    {
        throw new PendingException();
    }

    /**
     * @Then Sistem merubah pekerjaan tersebut dan menampilkan pesan :arg1
     */
    public function sistemMerubahPekerjaanTersebutDanMenampilkanPesan($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Given Melani ingin menghapus pekerjaannya yang ada di sistem yang bernama :arg1
     */
    public function melaniInginMenghapusPekerjaannyaYangAdaDiSistemYangBernama($arg1)
    {
        throw new PendingException();
    }

    /**
     * @When Melani melakukan DELETE request ke sistem :arg1
     */
    public function melaniMelakukanDeleteRequestKeSistem($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Then Sistem menghapus pekerjaan tersebut dan menampilkan pesan :arg1
     */
    public function sistemMenghapusPekerjaanTersebutDanMenampilkanPesan($arg1)
    {
        throw new PendingException();
    }

    /**
     * @Given Melani memiliki pekerjaan yang harus dimasukan ke sistem dengan judul <name>
     */
    public function melaniMemilikiPekerjaanYangHarusDimasukanKeSistemDenganJudulName()
    {
        throw new PendingException();
    }

    /**
     * @When Melani memasukan <description> pekerjaan dan <status>
     */
    public function melaniMemasukanDescriptionPekerjaanDanStatus()
    {
        throw new PendingException();
    }

    /**
     * @Then Sistem menyimpan pekerjaan tersebut dan menampilkan pesan <msg>
     */
    public function sistemMenyimpanPekerjaanTersebutDanMenampilkanPesanMsg(TableNode $table)
    {
        throw new PendingException();
    }
}
