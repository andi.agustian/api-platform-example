<?php

namespace App\Controller;

use App\Entity\Todos;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

Class TodosController extends Controller {

    public function createTodosAction()
    {
        $Todos = new Todos();

        $em = $this->getDoctrine()->getManager();
        $em->persist($Todos);
        $em->flush();
    }

    /**
     * @Route("/update/{id}", name="update")
     */
    public function updateTodosAction($id){

        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();

        // let's pretend that employee Tim has an ID of 6
        $todos = $em->getRepository('App:Todos')->find($id);

        debug($request);

        // $todos->setName();
        // $todos->setDescription();

        $em->persist($todos);
        $em->flush();

    }

     /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteTodosAction($id){

        $em = $this->getDoctrine()->getManager();

        $tim = $em->getRepository('App:Todos')->find($id);
        $em->remove($tim);

        // we still need to flush this change to the database
        $em->flush();
    }

}
