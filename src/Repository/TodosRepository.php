<?php
namespace App\Repository;
/**
 * Created by PhpStorm.
 * User: andy
 * Date: 28-Nov-18
 * Time: 10:36 PM
 */
use App\Entity\Todos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class TodosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Todos::class);
    }
}