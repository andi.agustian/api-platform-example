<?php
namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource
 * @ORM\Entity(repositoryClass="App\Repository\TodosRepository")
 */
Class Todos{

    /**
     * @var int The entity Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string name
     *
     * @ORM\Column
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @var string description
     *
     * @ORM\Column
     * @Assert\NotBlank
     */
    private $description;

    // /**
    //  * @var \DateTime $created_at created_at
    //  * @ORM\Column(type="datetime")
    //  * @Groups({"todos"})
    //  */
    // private $created_at;

    /**
     * @var string status
     *
     * @ORM\Column(options={"default":"pending"})
     * @Assert\NotBlank
     */
    private $status;

    // /**
    //  * Todos constructor.
    //  * @param \DateTime $created_at
    //  */
    // public function __construct(\DateTime $created_at)
    // {
    //     $created_at = new \DateTime("now");
    //     $this->created_at = $created_at;
    // }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    // /**
    //  * @return \DateTime
    //  */
    // public function getCreatedAt()
    // {
    //     return $this->created_at;
    // }

    // /**
    //  * @param \DateTime
    //  */
    // public function setCreatedAt(\DateTime $created_at)
    // {
    //     //$created_at = new \DateTime("now");
    //     $this->created_at = $created_at;
    // }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }




}
